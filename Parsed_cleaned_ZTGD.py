#!/usr/bin/env python
# coding: utf-8

# In[1]:


import requests
from bs4 import BeautifulSoup as bs
import json
from tqdm import tqdm


# In[2]:


headers = {
    'Accept-Encoding': 'gzip, deflate, sdch',
    'Accept-Language': 'en-US,en;q=0.8',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Cache-Control': 'max-age=0',
    'Connection': 'keep-alive',
}


# In[3]:


json_path = "/Users/Admin/Desktop/meta_project/ZIGD"
def parse_ZTGD_page(games_reviews, page_count):
    review_count = 0
    for i in range(len(games_reviews)):
        # name
        name = games_reviews[i].find('div', class_='gp-loop-content').h2.text
        symbol = name.find('(')
        if symbol == -1:
            pass
        else:
            name = name[:symbol-1]
        
        #link
#       link = games_reviews[i].find('div', class_='gp-post-thumbnail gp-loop-featured').find('a').get('href')
        link = games_reviews[i].find('div', class_='gp-loop-content').find('h2').find('a').get('href')
        
        #text of comment
        review = requests.get(link, allow_redirects=False, headers=headers)
        soup = bs(review.text, "html.parser")
        text = ''
        for p in soup.find_all('p'):
            text += p.text


        # score
        if games_reviews[i].find(class_='gp-rating-score'):
            score = games_reviews[i].find(class_='gp-rating-score').text.replace('\t', '').replace('\n', '')
        else:
            score = ""


        # date
        date = games_reviews[i].find('time').text


        with open(json_path+f"/page{page_count}_comment{review_count}.json", 'w') as f:
            json.dump({"name": name, "link": link, "text": text, "score": score, "date": date}, f)
        review_count += 1
    page_count += 1
    return page_count


# In[4]:


# first page have different url
page_count = 0
URL_TEMPLATE = "https://ztgd.com/category/reviews/"
r = requests.get(URL_TEMPLATE, headers=headers)
soup = bs(r.text, "html.parser")
games_reviews = soup.find('div', {'id': 'gp-left-column'}).find_all('section', {'itemtype': 'http://schema.org/Blog'})
page_count = parse_ZTGD_page(games_reviews, page_count)


# In[5]:


# url for other pages
paths = []
for i in tqdm(range (2,441)):
    paths.append(URL_TEMPLATE+"page/"+str(i))


# In[6]:


# main loop
page_count = 1
for url in tqdm(paths):
    r = requests.get(url, headers=headers)
    soup = bs(r.text, "html.parser")
    games_reviews = soup.find('div', {'id': 'gp-left-column'}).find_all('section', {'itemtype': 'http://schema.org/Blog'})
    page_count = parse_ZTGD_page(games_reviews, page_count)


# In[7]:


# check
with open('/Users/Admin/Desktop/meta_project/ZIGD/page2_comment9.json') as f:
    templates = json.load(f)
templates


# In[33]:


from pathlib import Path
import os


# In[34]:


# assign directory
directory = "C:/Users/Admin/Desktop/meta_project/ZTGD"


# In[35]:


names = Path(directory).glob('*.json')


# In[36]:


for n in tqdm.tqdm(names):
    with open(n) as f:
                data = json.load(f)
                name = data['name']
                comment = data['text']
                comment = comment.encode("ascii", "ignore")
                comment = comment.decode()
                comment = comment.replace("\\", "")
                comment = comment.lstrip()
                comment = comment.rstrip()
                comment = comment.replace("\"", "~")
                comment = comment.replace("\r", "")
                comment = comment.replace("\n", "")
                comment = comment.replace("\t", "")
                name = name.encode("ascii", "ignore")
                name = name.decode()
                name = name.replace("\\", "")
                name = name.lstrip()
                name = name.rstrip()
                name = name.replace("\"", "~")
                name = name.replace("\r", "")
                name = name.replace("\n", "")
                name = name.replace("\t", "")
                data['text'] = comment
                data['name'] = name
                with open(n, 'w') as outfile:
                    json.dump(data, outfile)


# In[ ]:




